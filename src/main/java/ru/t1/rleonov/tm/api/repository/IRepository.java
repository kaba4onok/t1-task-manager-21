package ru.t1.rleonov.tm.api.repository;

import java.util.Comparator;
import java.util.List;

public interface IRepository<M> {

    int getSize();

    M add(M model);

    List<M> findAll();

    List<M> findAll(Comparator comparator);

    boolean existsById(String id);

    M findOneById(String id);

    M findOneByIndex(Integer index);

    M remove(M model);

    M removeById (String id);

    M removeByIndex (Integer index);

    void clear();

}
