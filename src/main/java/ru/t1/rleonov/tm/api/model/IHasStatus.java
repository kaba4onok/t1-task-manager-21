package ru.t1.rleonov.tm.api.model;

import ru.t1.rleonov.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
