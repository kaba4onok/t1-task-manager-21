package ru.t1.rleonov.tm.repository;

import ru.t1.rleonov.tm.api.repository.IProjectRepository;
import ru.t1.rleonov.tm.model.Project;

public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    public Project create(final String userId, String name, String description) {
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        return add(project);
    }

    public Project create(final String userId, String name) {
        final Project project = new Project();
        project.setName(name);
        project.setUserId(userId);
        return add(project);
    }

}
