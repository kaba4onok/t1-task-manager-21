package ru.t1.rleonov.tm.command.user;

import ru.t1.rleonov.tm.api.service.IUserService;
import ru.t1.rleonov.tm.command.AbstractCommand;
import ru.t1.rleonov.tm.model.User;

public abstract class AbstractUserCommand extends AbstractCommand {

    protected IUserService getUserService() {
        return getServiceLocator().getUserService();
    }

    protected void showUser(final User user) {
        if (user == null) return;
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("EMAIL: " + user.getEmail());
        System.out.println("ROLE: " + user.getRole().getDisplayName());
    }

    @Override
    public String getArgument() {
        return null;
    }

}
