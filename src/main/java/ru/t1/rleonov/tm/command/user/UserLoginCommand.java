package ru.t1.rleonov.tm.command.user;

import ru.t1.rleonov.tm.enumerated.Role;
import ru.t1.rleonov.tm.util.TerminalUtil;

public final class UserLoginCommand extends AbstractUserCommand {

    private final String NAME = "login";

    private final String DESCRIPTION = "User login.";

    @Override
    public void execute() {
        System.out.println("[USER LOGIN]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        getAuthService().login(login, password);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public Role[] getRoles() {
        return null;
    }

}
