package ru.t1.rleonov.tm.command.system;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    public static final String ARGUMENT = "-a";

    public static final String NAME = "about";

    public static final String DESCRIPTION = "Show application info.";

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("Task Manager");
        System.out.println("Developer: Roman Leonov");
        System.out.println("Gitlab: https://gitlab.com/kaba4onok/");
        System.out.println("E-mail: rleonov@t1-consulting.ru");
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
